﻿Imports System.IO
Imports System.Xml.Serialization

Namespace Utils
    Public Class XmlParser
        Public Shared Function LoadXml(Of T)(ByVal xml As String) As T
            Dim returnedXmlClass As T = Nothing
            Try
                xml = xml.Trim().Replace("<STA","<xml").Replace("</STA>","</xml>")
                Using reader As TextReader = New StringReader(xml)
                    Try
                        returnedXmlClass = CType(New XmlSerializer(GetType(T)).Deserialize(reader), T)
                    Catch exception As InvalidOperationException
                        Console.WriteLine(exception.Message)
                    End Try
                End Using

            Catch ex As Exception
                Console.WriteLine(ex.Message)
            End Try

            Return returnedXmlClass
        End Function
    End Class
End NameSpace