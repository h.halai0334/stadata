﻿Namespace Enums
    Public Enum EnumOrizzontamento
        OrzUserDefined = 0
        OrzLegnoTavolato = 1
        OrzLegnoTavolatoDoppio = 2
        OrzLegnoSoletta = 3
        OrzPutrelleTavelloni = 4
        OrzPutrelleVoltini = 5
        OrzLateroCementizio = 6
        OrzImpalcatoRigido = 7 
        OrzLegnoSoletta2 = 8 'Dippelbaumdecke
        OrzLamieraGrecata = 9
    End Enum
End NameSpace