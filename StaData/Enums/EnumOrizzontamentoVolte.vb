﻿Namespace Enums
    Public Enum EnumOrizzontamentoVolte
        OrzVoltaUserDefined = 0
        OrzVoltaBotte = 1
        OrzVoltaBottePadiglione = 2
        OrzVoltaCrociera = 3
        OrzVoltaPadiglione = 4
        OrzVoltaVela = 5
    End Enum
End NameSpace