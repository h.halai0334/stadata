﻿Namespace Enums
    Public Enum EnumElements
        ElmNone = 0
        ElmPilastro = 1
        ElmForo = 4
        ElmSolaio = 5
        ElmVolta = 6
        ElmParete = 9
        ElmNodoElemento = 10
        ElmAsta = 11
        ElmCarico = 12
        ElmBalcone = 3
        ElmDxf = 13
        ElmBloccoCinema = 20
        ElmFalda = 21
        ElmProfilo = 22
        ElmGrafica = 23
        ElmBlocco3D = 24
        ElmSporgenza = 25 
        ElmRinforzoParete = 26 
        ElmRinforzoSolaio = 27 
        ElmRinforzoNodo = 28
        ElmComposto = 29 
        ElmSemplice = 30
        ElmRinforzoVolta = 31
        ElmRinforzoFalda = 32 
        ElmLungApp = 33 
        ElmPlinto = 34
    End Enum
End NameSpace