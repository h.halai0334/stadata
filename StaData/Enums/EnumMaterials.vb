﻿Namespace Enums
    Public Enum EnumMaterials
        MatNone=0
        MatMuratura = 1
        MatCalcestruzzo = 2
        MatAcciaio = 3
        MatLegno = 4
        MatFrp = 5
        MatStruttureAcciaio = 6
        MatBiella = 10
        MatAltro = 99
    End Enum
End NameSpace