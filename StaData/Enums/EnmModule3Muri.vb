﻿Namespace Enums
    Public Enum EnmModule3Muri
        EnmNone = 0
        EnmAcSolai = 101
        EnmAcVento = 102
        EnmAcNeve = 103
        EnmAcSolaiQk = 106
        EnmVcaClsOpera = 201          
        EnmRmArchitAcc = 401
        EnmRmArchitMur = 402      
        EnmRmAperture = 404        
        EnmEsTamp = 501      
        EnmEsSolaiL = 503
        EnmEsSolaiP = 504       
        EnmRpManutenzz = 702
        EnmRpCapitolato = 703     
    End Enum
End NameSpace