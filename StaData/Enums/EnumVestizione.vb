﻿Namespace Enums
    Public Enum EnumVestizione
        VstNonDefinito = 0
        VstPannello = 1
        VstTraveNl = 2
        VstTraveCa = 3
        VstCatena = 4
        VstSettoCa = 5
        VstBiella = 6
        VstPilastroMur = 7
        VstPilastroNl = 8
        VstPilastroCa = 9
        VstPannelloTraveNl = 12     
        VstPannelloCordoloCa = 13  
        VstPannelloCatena = 14    
        VstNonSelezionabile = 15 
        VstTamponamento = 16 
        VstPlinto = 17
    End Enum
End NameSpace