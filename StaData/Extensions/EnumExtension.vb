﻿Imports System.Runtime.CompilerServices

Namespace Extensions

    Public Module EnumExtension
        <Extension()>
        Public Function ToInt(Of T As IConvertible)(ByVal source As T) As Integer
            If Not GetType(T).IsEnum Then Throw New ArgumentException("T must be an enumerated type")
            Return CInt(CType(source, IConvertible))
        End Function
    End Module
End NameSpace