﻿Imports IniParser
Imports IniParser.Model

Namespace Settings
    Public Class SettingManager
        Private Const SECTION_STA_TXT As String = "STA_TXT"
        Private Const SECTION_DOMANDE As String = "DOMANDE"
        Private Const SECTION_LAVORAZIONI As String = "LAVORAZIONI"
        Private Const SECTION_STA_TXT_INP_DESCRIZIONE As String = "inp_Descrizione"
        Private Const SECTION_STA_TXT_INP_TITOLO As String = "inp_Titolo"
        Private Const SECTION_STA_TXT_INP_CATEGORIAINTERVENTO As String = "inp_CategoriaIntervento"
        Private Const SECTION_STA_TXT_INP_TIPOELEMENTO As String = "inp_TipoElemento"
        Private Const SECTION_STA_TXT_INP_TIPOVESTIZIONE As String = "inp_TipoVestizione"
        Private Const SECTION_STA_TXT_INP_REQUISITOS As String = "inp_Requisitos"
        Private Const SECTION_STA_TXT_INP_SUBREQUISITOS As String = "inp_SubRequisitos"
        Private Const SECTION_STA_TXT_OUT_INTERVENTO As String = "out_Intervento"
        Private Const SECTION_STA_TXT_OUT_TREE As String = "out_tree"
        Private Const SECTION_STA_TXT_OUT_NOTE As String = "out_Note"
        Private Const SECTION_DOMANDE_NFACCE As String = "NFacce"
        Private Const SECTION_DOMANDE_NSTRATI As String = "NStrati"
        Private Const SECTION_LAVORAZIONI_TATALE As String = "Tatale"
        Private Const SECTION_LAVORAZIONI_NUMERO As String = "Numero"
        Private Const SECTION_LAVORAZIONI_DESCRIZIONE As String = "Descrizione"
        Private Const SECTION_LAVORAZIONI_N As String = "N"
        Private Const SECTION_LAVORAZIONI_QUANTITA As String = "Quantita"
        Private Const SECTION_LAVORAZIONI_PREZZO As String = "Prezzo"
        Private Const SECTION_LAVORAZIONI_TOTALE As String = "Toatale"
        Public Function SaveSetting(fileName As String, jobSettingModel As JobSettingModel) As Boolean
            Try
                Dim parser As FileIniDataParser = New FileIniDataParser()
                Dim index As Integer = 1
                Dim data As IniData = New IniData()
                data.Sections.Add(New SectionData(SECTION_STA_TXT))
                data.Sections.Add(New SectionData(SECTION_DOMANDE))
                data.Sections.Add(New SectionData(SECTION_LAVORAZIONI))
                data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_DESCRIZIONE) = jobSettingModel.StaSetting.inp_Descrizione
                data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_TITOLO) = jobSettingModel.StaSetting.inp_Titolo
                data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_CATEGORIAINTERVENTO) = jobSettingModel.StaSetting.inp_CategoriaIntervento.ToString()
                For Each item As Integer In jobSettingModel.StaSetting.inp_TipoElemento
                    data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_TIPOELEMENTO + "_" + index.ToString()) = item.ToString()
                    index += 1
                Next
                index = 1
                For Each item As Integer In jobSettingModel.StaSetting.inp_TipoVestizione
                    data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_TIPOVESTIZIONE + "_" + index.ToString()) = item.ToString()
                    index += 1
                Next
                index = 1
                For Each item As Integer In jobSettingModel.StaSetting.inp_Requisitos
                    data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_REQUISITOS + "_" + index.ToString()) = item.ToString()
                    index += 1
                Next
                index = 1
                For Each item As String In jobSettingModel.StaSetting.inp_SubRequisitos
                    data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_SUBREQUISITOS + "_" + index.ToString()) = item
                    index += 1
                Next
                index = 1

                data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_OUT_INTERVENTO) = jobSettingModel.StaSetting.out_Intervento
                data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_OUT_TREE) = jobSettingModel.StaSetting.out_tree
                data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_OUT_NOTE) = jobSettingModel.StaSetting.out_Note
                data.Sections.Item(SECTION_DOMANDE).Item(SECTION_DOMANDE_NFACCE) = jobSettingModel.Stadomande.NFacce.ToString()
                data.Sections.Item(SECTION_DOMANDE).Item(SECTION_DOMANDE_NSTRATI) = jobSettingModel.Stadomande.NStrati.ToString()
                data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_TATALE) = jobSettingModel.Stalavorazioni.Tatale.ToString()
                data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_NUMERO) = jobSettingModel.Stalavorazioni.Numero.ToString()
                For Each item As STAItemModel In jobSettingModel.Stalavorazioni.Items
                    data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_DESCRIZIONE + "_" + index.ToString()) = item.Descrizione
                    data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_N + "_" + index.ToString()) = item.N.ToString()
                    data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_QUANTITA + "_" + index.ToString()) = item.Quantita.ToString()
                    data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_PREZZO + "_" + index.ToString()) = item.Prezzo.ToString()
                    data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_TOTALE + "_" + index.ToString()) = item.Toatale.ToString()
                    index += 1
                Next
                parser.WriteFile(fileName, data)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
                Return False
            Finally
            End Try
            Return True
        End Function

        Public Function ReadSetting(fileName As String) As JobSettingModel
            Dim jobSettingModel As JobSettingModel = New JobSettingModel()
            Try
                Dim parser As FileIniDataParser = New FileIniDataParser()
                Dim data As IniData = New IniData()
                data = parser.ReadFile(fileName)
                jobSettingModel.Initialize()
                If data IsNot Nothing Then
                    Dim index As Integer = 1
                    jobSettingModel.StaSetting.inp_Descrizione = data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_DESCRIZIONE)
                    jobSettingModel.StaSetting.inp_Titolo = data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_TITOLO)
                    Integer.TryParse(data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_CATEGORIAINTERVENTO), jobSettingModel.StaSetting.inp_CategoriaIntervento)

                    While data.Sections.Item(SECTION_STA_TXT).ContainsKey(SECTION_STA_TXT_INP_TIPOELEMENTO + "_" + index.ToString())
                        Dim element As Integer = 0
                        Integer.TryParse(data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_TIPOELEMENTO + "_" + index.ToString()), element)
                        jobSettingModel.StaSetting.inp_TipoElemento.Add(element)
                        index += 1
                    End While
                    index = 1
                    While data.Sections.Item(SECTION_STA_TXT).ContainsKey(SECTION_STA_TXT_INP_SUBREQUISITOS + "_" + index.ToString())
                        jobSettingModel.StaSetting.inp_SubRequisitos.Add(data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_SUBREQUISITOS + "_" + index.ToString()).ToString())
                        index += 1
                    End While
                    index = 1
                    While data.Sections.Item(SECTION_STA_TXT).ContainsKey(SECTION_STA_TXT_INP_TIPOVESTIZIONE + "_" + index.ToString())
                        Dim element As Integer = 0
                        Integer.TryParse(data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_TIPOVESTIZIONE + "_" + index.ToString()), element)
                        jobSettingModel.StaSetting.inp_TipoVestizione.Add(element)
                        index += 1
                    End While
                    index = 1
                    While data.Sections.Item(SECTION_STA_TXT).ContainsKey(SECTION_STA_TXT_INP_REQUISITOS + "_" + index.ToString())
                        Dim element As Integer = 0
                        Integer.TryParse(data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_INP_REQUISITOS + "_" + index.ToString()), element)
                        jobSettingModel.StaSetting.inp_Requisitos.Add(element)
                        index += 1
                    End While
                    index = 1
                    jobSettingModel.StaSetting.out_Intervento = data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_OUT_INTERVENTO)
                    jobSettingModel.StaSetting.out_tree = data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_OUT_TREE)
                    jobSettingModel.StaSetting.out_Note = data.Sections.Item(SECTION_STA_TXT).Item(SECTION_STA_TXT_OUT_NOTE)

                    Integer.TryParse(data.Sections.Item(SECTION_DOMANDE).Item(SECTION_DOMANDE_NFACCE), jobSettingModel.Stadomande.NFacce)
                    Integer.TryParse(data.Sections.Item(SECTION_DOMANDE).Item(SECTION_DOMANDE_NSTRATI), jobSettingModel.Stadomande.NStrati)

                    Decimal.TryParse(data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_TATALE), jobSettingModel.Stalavorazioni.Tatale)
                    Integer.TryParse(data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_NUMERO), jobSettingModel.Stalavorazioni.Numero)
                    While data.Sections.Item(SECTION_LAVORAZIONI).ContainsKey(SECTION_LAVORAZIONI_DESCRIZIONE + "_" + index.ToString())
                        Dim item As STAItemModel = New STAItemModel()
                        item.Descrizione = data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_DESCRIZIONE + "_" + index.ToString())
                        Decimal.TryParse(data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_N + "_" + index.ToString()), item.N)
                        Decimal.TryParse(data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_QUANTITA + "_" + index.ToString()), item.Quantita)
                        Decimal.TryParse(data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_PREZZO + "_" + index.ToString()), item.Prezzo)
                        Decimal.TryParse(data.Sections.Item(SECTION_LAVORAZIONI).Item(SECTION_LAVORAZIONI_TOTALE + "_" + index.ToString()), item.Toatale)
                        index += 1
                        jobSettingModel.Stalavorazioni.Items.Add(item)
                    End While
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            Finally
            End Try
            Return jobSettingModel
        End Function
    End Class

    Public Class JobSettingModel
        Public StaSetting As STASettingModel
        Public Stadomande As STADOMANDEModel
        Public Stalavorazioni As STALAVORAZIONIModel
        Public Sub New()
            Initialize()
        End Sub
        Public Sub Initialize()
            Stadomande = New STADOMANDEModel()
            Stalavorazioni = New STALAVORAZIONIModel()
            StaSetting = New STASettingModel()
        End Sub
    End Class
    Public Class STASettingModel
        Public inp_Descrizione As String
        Public inp_Titolo As String
        Public inp_CategoriaIntervento As Integer
        Public inp_TipoElemento As List(Of Integer) = New List(Of Integer)()
        Public inp_TipoVestizione As List(Of Integer) = New List(Of Integer)()
        Public out_Intervento As String
        Public out_tree As String
        Public out_Note As String
        Public inp_Requisitos As List(Of Integer) = New List(Of Integer)()
        Public inp_SubRequisitos As List(Of String) = New List(Of String)()
    End Class

    Public Class STADOMANDEModel 
        Public NFacce As Integer
        Public NStrati As Integer
    End Class

    Public Class STALAVORAZIONIModel 
        Public Tatale As Decimal
        Public Numero As Integer
        Public Items AS List(of STAItemModel) = New List(Of STAItemModel)()
    End Class
    Public Class STAItemModel 
        Public Descrizione As String
        Public N As Decimal
        Public Quantita As Decimal
        Public Prezzo As Decimal
        Public Toatale As Decimal
    End Class
End NameSpace