﻿Imports System.Xml.Serialization

Namespace XmlModels.TreeModel
    <XmlRoot(ElementName:="INTERVENTO")>
    Public Class Intervento
        <XmlAttribute(AttributeName:="key")>
        Public Property Key As String
        <XmlText>
        Public Property Text As String
    End Class

    <XmlRoot(ElementName:="TREE")>
    Public Class Tree
        <XmlText>
        Public Property Text As String
        <XmlAttribute(AttributeName:="key")>
        Public Property Key As String
        <XmlElement(ElementName:="INTERVENTO")>
        Public Property Intervento As List(Of Intervento)
    End Class

    <XmlRoot(ElementName:="xml")>
    Public Class TreeModel
        <XmlElement(ElementName:="TREE")>
        Public Property Tree As List(Of Tree)
        <XmlAttribute(AttributeName:="tree")>
        Public Property Text As String
        <XmlAttribute(AttributeName:="version")>
        Public Property Version As String
    End Class
End NameSpace