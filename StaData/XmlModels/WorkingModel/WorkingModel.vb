﻿
Imports System.Xml.Serialization

Namespace XmlModels.WorkingModel

    
    <XmlRoot(ElementName:="DOMANDA")>
    Public Class Domanda
        <XmlAttribute(AttributeName:="nome")>
        Public Property Nome As String
        <XmlAttribute(AttributeName:="key")>
        Public Property Key As String
        <XmlText>
        Public Property Text As String
    End Class

    <XmlRoot(ElementName:="DOMANDE")>
    Public Class Domande
        <XmlElement(ElementName:="DOMANDA")>
        Public Property Domanda As List(Of Domanda)
    End Class

    <XmlRoot(ElementName:="PREZZO")>
    Public Class Prezzo
        <XmlAttribute(AttributeName:="type")>
        Public Property Type As String
        <XmlText>
        Public Property Text As String
    End Class

    <XmlRoot(ElementName:="LAVORAZIONE")>
    Public Class Lavorazione
        <XmlElement(ElementName:="COEFF")>
        Public Property Coeff As String
        <XmlElement(ElementName:="PREZZO")>
        Public Property Prezzo As Prezzo
        <XmlAttribute(AttributeName:="nome")>
        Public Property Nome As String
        <XmlAttribute(AttributeName:="key")>
        Public Property Key As String
    End Class

    <XmlRoot(ElementName:="LAVORAZIONI")>
    Public Class Lavorazioni
        <XmlElement(ElementName:="LAVORAZIONE")>
        Public Property Lavorazione As List(Of Lavorazione)
    End Class

    <XmlRoot(ElementName:="xml")>
    Public Class WorkingModel
        <XmlElement(ElementName:="DOMANDE")>
        Public Property Domande As Domande
        <XmlElement(ElementName:="LAVORAZIONI")>
        Public Property Lavorazioni As Lavorazioni
        <XmlAttribute(AttributeName:="banca")>
        Public Property Banca As String
        <XmlAttribute(AttributeName:="versione")>
        Public Property Versione As String
    End Class

End NameSpace