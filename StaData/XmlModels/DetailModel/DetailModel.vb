﻿
Imports System.Xml.Serialization

Namespace XmlModels.DetailModel
    <XmlRoot(ElementName := "APPLICAZIONE")>
    Public Class Applicazione
        <XmlAttribute(AttributeName := "Category")>
        Public Property Category As String

        <XmlAttribute(AttributeName := "SubCategory")>
        Public Property SubCategory As String
    End Class

    <XmlRoot(ElementName := "SOTTOREQUISITO")>
    Public Class Sottorequisito
        <XmlAttribute(AttributeName := "key")>
        Public Property Key As String
    End Class

    <XmlRoot(ElementName := "REQUISITO")>
    Public Class Requisito
        <XmlElement(ElementName := "SOTTOREQUISITO")>
        Public Property Sottorequisito As List(Of Sottorequisito)

        <XmlAttribute(AttributeName := "Category")>
        Public Property Category As String
    End Class

    <XmlRoot(ElementName := "ELEMENTO")>
    Public Class Elemento
        <XmlElement(ElementName := "APPLICAZIONE")>
        Public Property Applicazione As List(Of Applicazione)

        <XmlElement(ElementName := "REQUISITO")>
        Public Property Requisito As Requisito
    End Class

    <XmlRoot(ElementName := "SCHEDACALCOLO")>
    Public Class Schedacalcolo
        <XmlAttribute(AttributeName := "Group")>
        Public Property Group As String

        <XmlAttribute(AttributeName := "Module")>
        Public Property [Module] As String
    End Class

    <XmlRoot(ElementName := "INTERVENTO")>
    Public Class Intervento
        <XmlElement(ElementName := "ELEMENTO")>
        Public Property Elemento As List(Of Elemento)

        <XmlElement(ElementName := "DESCRIZIONE")>
        Public Property Descrizione As String

        <XmlElement(ElementName := "BIBLIOGRAFIA")>
        Public Property Bibliografia As String

        <XmlElement(ElementName := "SOLUZIONI")>
        Public Property Soluzioni As String

        <XmlElement(ElementName := "LAVORAZIONI")>
        Public Property Lavorazioni As String

        <XmlElement(ElementName := "SCHEDACALCOLO")>
        Public Property Schedacalcolo As Schedacalcolo

        <XmlAttribute(AttributeName := "nome")>
        Public Property Nome As String

        <XmlAttribute(AttributeName := "key")>
        Public Property Key As String
    End Class

    <XmlRoot(ElementName := "xml")>
    Public Class DetailModel
        <XmlElement(ElementName := "INTERVENTO")>
        Public Property Intervento As List(Of Intervento)

        <XmlAttribute(AttributeName := "banca")>
        Public Property Banca As String

        <XmlAttribute(AttributeName := "versione")>
        Public Property Versione As String
    End Class
End NameSpace