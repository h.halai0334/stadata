﻿Imports StaData.Enums

Public Class frmMain
    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        Dim directoryPath As String = IO.Path.Combine(Application.StartupPath, "Data")
        Dim filePath As String = IO.Path.Combine(directoryPath, "test.cds")
        Dim materials As List(Of EnumMaterials) = New List(Of EnumMaterials)
        materials.Add(EnumMaterials.MatNone)
        materials.Add(EnumMaterials.MatMuratura)
        Dim subRequisitoKeys As List(Of String) = New List(Of String)()
        subRequisitoKeys.Add("1")
        subRequisitoKeys.Add("4")
        Dim categories As List(Of EnumElements) = New List(Of EnumElements)()
        categories.Add(EnumElements.ElmAsta)
        categories.Add(EnumElements.ElmAsta)
        Dim subCategories As List(Of Integer) = New List(Of Integer)()
        subCategories.Add(1)
        subCategories.Add(1)
        Dim form As frmSta =
                New frmSta(filePath, directoryPath, EnumMappaInterventi.Elemento, materials, subRequisitoKeys, categories, subCategories)
        form.ShowDialog()
    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click
        Dim directoryPath As String = IO.Path.Combine(Application.StartupPath, "Data")
        Dim filePath As String = IO.Path.Combine(directoryPath, "test.cds")
        Dim form As frmSta =
        New frmSta(filePath, directoryPath)
        form.ShowDialog()

    End Sub
End Class