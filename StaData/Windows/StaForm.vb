﻿Option Strict On
Option Explicit On
Option Infer Off

Imports System.IO
Imports StaData.Enums
Imports StaData.Extensions
Imports StaData.Settings
Imports StaData.Utils
Imports StaData.XmlModels.DetailModel
Imports StaData.XmlModels.TreeModel
Imports StaData.XmlModels.WorkingModel


Public Class frmSta
    Private pJobType AS EnumStatoOperativo
    Private pFilePath AS String
    Private pDirectoryPath AS String
    Private pCategories AS List(of EnumElements)
    Private pSubCategories AS List(of Integer)
    Private pInterventionTypes AS EnumMappaInterventi = EnumMappaInterventi.None
    Private pMaterials AS List(of EnumMaterials)
    Private pSubRequisitos AS List(of String)
    Private pSelectedTreeKey As String = ""
    Private pSelectedNodeKey As String = ""
    Public Sub New(filePath AS String, directoryPath As String)
        InitializeComponent()
        
        pJobType = EnumStatoOperativo.edtMODIFICA
        pFilePath = filePath
        pDirectoryPath = directoryPath
        If File.Exists(filePath) Then
            Dim settingManager As SettingManager = New SettingManager()
            pSetting = settingManager.ReadSetting(filePath)
            pCategories = pSetting.StaSetting.inp_TipoElemento.Select(function(item) CType(item,EnumElements)).ToList()
            pSubCategories = pSetting.StaSetting.inp_TipoVestizione
            pInterventionTypes = CType(pSetting.StaSetting.inp_CategoriaIntervento, EnumMappaInterventi)
            pMaterials = pSetting.StaSetting.inp_Requisitos.Select(Function(item) CType(item, EnumMaterials)).ToList()
            pSelectedTreeKey = pSetting.StaSetting.out_tree
            pSelectedNodeKey = pSetting.StaSetting.out_Intervento
            pSubRequisitos = pSetting.StaSetting.inp_SubRequisitos
            txtNote.Text = pSetting.StaSetting.out_Note
            txtTitle.Text = pSetting.StaSetting.inp_Descrizione
        Else
            MessageBox.Show("No File Found : " + filePath)
        End If
        If Directory.Exists(pDirectoryPath) Then
        Else
            MessageBox.Show("No Directory Found : " + filePath)
        End If
        pStaDataHelp = New StaData_HELP.StaData_HELP(Me)
        pStaDataDict = New StaData_DICT.StaData_DICT(Me)
        pStaDataLib = New StaData_LIBRARY.StaData_LIBRARY.Cls_Form(Me)
        DATA_DIRECTORY = pDirectoryPath
        pStaDataDict.LoadDictionaryFromFileXML(Path.Combine(XML_DIRECTORY, ITALIANO_XML_FILE), "STA")
        If Directory.Exists(DATA_DIRECTORY) Then
            pWorkingModel = XmlParser.LoadXml(Of WorkingModel)(File.ReadAllText(Path.Combine(DATA_DIRECTORY, WORKING_XML_FILE)))
            LoadTree()
            SelectTreeNode(pSelectedTreeKey, pSelectedNodeKey, Nothing)
        End If

    End Sub

    Private Sub SelectTreeNode(treeKey As String, childKey As String, parentNode As TreeNode)
        If parentNode Is Nothing Then
            For Each node As TreeNode In tvXml.Nodes
                If node.Name = treeKey Then
                    SelectTreeNode(treeKey, childKey, node)
                End If
            Next
        Else
            For Each node As TreeNode In parentNode.Nodes
                If node.Name = childKey And parentNode.Name = treeKey Then
                    tvXml.SelectedNode = node
                    Exit For
                ElseIf node.Name = treeKey Then
                    SelectTreeNode(treeKey, childKey, node)
                End If
            Next
        End If
    End Sub

    Public Sub New(filePath As String, directoryPath As String,
                   interventionType As EnumMappaInterventi,
                   materials As List(Of EnumMaterials),
                   subRequisito As List(Of String),
                   category As List(Of EnumElements), subCategory As List(Of Integer))
        InitializeComponent()

        pJobType = EnumStatoOperativo.edtINSERIMENTO
        pFilePath = filePath
        pDirectoryPath = directoryPath
        pInterventionTypes = interventionType
        pMaterials = materials
        pSubRequisitos = subRequisito
        pCategories = category
        pSubCategories = subCategory

        pStaDataHelp = New StaData_HELP.StaData_HELP(Me)
        pStaDataDict = New StaData_DICT.StaData_DICT(Me)
        pStaDataLib = New StaData_LIBRARY.StaData_LIBRARY.Cls_Form(Me)
        DATA_DIRECTORY = pDirectoryPath
        pStaDataDict.LoadDictionaryFromFileXML(Path.Combine(DATA_DIRECTORY, ITALIANO_XML_FILE), "STA")
        pWorkingModel = XmlParser.LoadXml(Of WorkingModel)(File.ReadAllText(Path.Combine(XML_DIRECTORY, WORKING_XML_FILE)))
        If Directory.Exists(directoryPath) Then
            LoadTree()
        Else
            Throw New ArgumentException("No Directory Found", directoryPath)
        End If
    End Sub

    Private ReadOnly pSetting As JobSettingModel = New JobSettingModel()
    Private Const TYPE_ELEMENT As String = "Element"
    Private Const TYPE_CONNECTION As String = "Connection"
    Private DATA_DIRECTORY As String = ""
    Private XML_DIRECTORY As String = "Data"
    Private Const CONNECTION_TREEVIEW_FILE As String = "BANCA DATI_Connection TreeView Specification.xml"
    Private Const CONNECTION_XML_FILE As String = "BANCA DATI_Connection XML Specification.xml"
    Private Const ELEMENT_TREEVIEW_FILE As String = "BANCA DATI_Element TreeView Specification.xml"
    Private Const ELEMENT_XML_FILE As String = "BANCA DATI_Element XML Specification.xml"
    Private Const WORKING_XML_FILE As String = "BANCADATI_Working XML Specification.xml"
    Private Const ITALIANO_XML_FILE As String = "Italiano.xml"
    Private ptreeModel As TreeModel
    Private pWorkingModel As WorkingModel
    Private pdetailModel As DetailModel
    Private pStaDataLib As StaData_LIBRARY.StaData_LIBRARY.Cls_Form
    Private pStaDataHelp As StaData_HELP.StaData_HELP
    Private pStaDataDict As StaData_DICT.StaData_DICT

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetLanguage()
    End Sub

    Private Sub SetLanguage()
        lblDescription.Text = pStaDataDict.GetXMLText("PRJ_DESC")
        '        lblNote.Text = pStaDataDict.GetXMLText("PRJ_DESC")
        btnOk.Text = pStaDataDict.GetXMLText("DLL37")
        btnCancel.Text = pStaDataDict.GetXMLText("DLL38")
        tpDescription.Text = pStaDataDict.GetXMLText("PRJ_DESC")
        '        tpBibliography.Text = pStaDataDict.GetXMLText("PRJ_DESC")
        tpComputation.Text = pStaDataDict.GetXMLText("PRJ_CALC")
        '        tpSolution.Text = pStaDataDict.GetXMLText("PRJ_DESC")
        '        tpInterventionArea.Text = pStaDataDict.GetXMLText("PRJ_DESC")
        dgvDataA.Columns(0).HeaderText = pStaDataDict.GetXMLText("PRJ_DESC")
        dgvDataA.Columns(1).HeaderText = pStaDataDict.GetXMLText("OUTDETAIL_No")
        dgvItems.Columns(0).HeaderText = pStaDataDict.GetXMLText("PRJ_DESC")
        dgvItems.Columns(1).HeaderText = pStaDataDict.GetXMLText("OUTDETAIL_No")
        '        dgvItems.Columns(2).HeaderText = pStaDataDict.GetXMLText("OUTDETAIL_No")
        '        dgvItems.Columns(3).HeaderText = pStaDataDict.GetXMLText("OUTDETAIL_No")
        '        dgvItems.Columns(4).HeaderText = pStaDataDict.GetXMLText("OUTDETAIL_No")
    End Sub

    Private Sub LoadComputation(ids As List(Of String))
        dgvDataA.Rows.Clear()
        dgvItems.Rows.Clear()
        dgvDataA.Rows.Add("No of Faces", pWorkingModel.Domande.Domanda.Count)
        dgvDataA.Rows.Add("No of Layers", pWorkingModel.Lavorazioni.Lavorazione.Count)
        pSetting.Stadomande.NFacce = pWorkingModel.Domande.Domanda.Count
        pSetting.Stadomande.NStrati = pWorkingModel.Lavorazioni.Lavorazione.Count
        Dim total As Decimal = pSetting.Stadomande.NFacce + pSetting.Stadomande.NStrati
        Dim items As List(Of Lavorazione) = pWorkingModel.Lavorazioni.Lavorazione.Where(Function(lavorazione) ids.Contains(lavorazione.Key)).ToList()
        pSetting.Stalavorazioni.Numero = items.Count
        pSetting.Stalavorazioni.Tatale = 0
        pSetting.StaSetting.inp_CategoriaIntervento = pInterventionTypes
        Dim index As Integer = 0
        For Each item As Lavorazione In items
            Dim itemText As String = pStaDataDict.GetXMLText(item.Nome)
            If String.IsNullOrWhiteSpace(itemText) Then
                itemText = item.Nome
            End If
            If index < pSetting.Stalavorazioni.Items.Count Then
                Dim staItem As STAItemModel = pSetting.Stalavorazioni.Items(index)
                dgvItems.Rows.Add(staItem.Descrizione, staItem.N, staItem.Quantita, staItem.Prezzo, staItem.Toatale)
            Else
                Dim price As Decimal = Decimal.Parse(item.Prezzo.Text)
                dgvItems.Rows.Add(itemText, total, 1, price, total * price)
                Dim staItem As STAItemModel = New STAItemModel()
                staItem.Descrizione = itemText
                staItem.N = total
                staItem.Prezzo = price
                staItem.Quantita = 1
                staItem.Toatale = total * price
                pSetting.Stalavorazioni.Items.Add(staItem)
            End If
            index += 1
        Next
    End Sub
    Private Function RemoveInterventionAndTree(index As Integer) As Integer
        Dim item As XmlModels.DetailModel.Intervento = pdetailModel.Intervento.Item(index)
        pdetailModel.Intervento.Remove(item)
        For Each tree As Tree In ptreeModel.Tree
            For i As Integer = 0 To tree.Intervento.Count - 1
                If i > tree.Intervento.Count - 1 Then
                    Exit For
                End If
                Dim intervento As XmlModels.TreeModel.Intervento = tree.Intervento.Item(i)
                If intervento.Key.Equals(item.Key) Then
                    tree.Intervento.RemoveAt(i)
                    i -= 1
                    Continue For
                End If
            Next
        Next
        index -= 1
        Return index
    End Function

    Private Sub LoadTree()
        ptreeModel = Nothing
        tvXml.Nodes.Clear()
        If pInterventionTypes = EnumMappaInterventi.Elemento Then
            ptreeModel = XmlParser.LoadXml(Of TreeModel)(File.ReadAllText(Path.Combine(DATA_DIRECTORY,
                                                                                        ELEMENT_TREEVIEW_FILE)))
            pdetailModel = XmlParser.LoadXml(Of DetailModel)(File.ReadAllText(Path.Combine(DATA_DIRECTORY,
                                                                                            ELEMENT_XML_FILE)))
        ElseIf pInterventionTypes = EnumMappaInterventi.Collegamento Then
            ptreeModel = XmlParser.LoadXml(Of TreeModel)(File.ReadAllText(Path.Combine(DATA_DIRECTORY,
                                                                                        CONNECTION_TREEVIEW_FILE)))
            pdetailModel = XmlParser.LoadXml(Of DetailModel)(File.ReadAllText(Path.Combine(DATA_DIRECTORY,
                                                                                            CONNECTION_XML_FILE)))
        End If
        If ptreeModel Is Nothing Then
            Return
        End If
        For i As Integer = 0 To pdetailModel.Intervento.Count - 1
            If i > pdetailModel.Intervento.Count - 1 Then
                Exit For
            End If
            Dim item As XmlModels.DetailModel.Intervento = pdetailModel.Intervento.Item(i)
            Dim isRemove As Boolean = False
            For elementIndex As Integer = 0 To item.Elemento.Count - 1
                Dim element As Elemento = item.Elemento.Item(elementIndex)
                Dim application As Applicazione = element.Applicazione.FirstOrDefault()
                Dim requisito As Requisito = element.Requisito
                Dim material As EnumMaterials = pMaterials.Item(elementIndex)
                Dim category As EnumElements = pCategories.Item(elementIndex)
                Dim subCategory As Integer = pSubCategories.Item(elementIndex)
                Dim requisitoKey As String = pSubRequisitos.Item(elementIndex)
                If Not material = EnumMaterials.MatNone Then
                    If Not requisito.Category = material.ToInt().ToString() Then
                        isRemove = True
                        Exit For
                    End If
                End If
                If Not category = EnumElements.ElmNone Then
                    If Not application.Category = category.ToInt().ToString() Then
                        isRemove = True
                        Exit For
                    End If
                End If
                If Not subCategory = 0 Then
                    If Not application.SubCategory = subCategory.ToString() Then
                        isRemove = True
                        Exit For
                    End If
                End If
                If Not String.IsNullOrWhiteSpace(requisitoKey) And Not requisitoKey = "0" Then
                    If Not requisito.Sottorequisito.Any(Function(sottorequisito) sottorequisito.Key = requisitoKey) Then
                        isRemove = True
                        Exit For
                    End If
                End If
            Next
            If isRemove Then
                i = RemoveInterventionAndTree(i)
                Continue For
            End If
        Next
        Dim index As Int32 = 1
        Dim previousNode As TreeNode = Nothing
        For Each tree As Tree In ptreeModel.Tree
            If Not tree.Intervento.Any() Then
                Continue For
            End If
            Dim items As String() = tree.Text.Split(";"c)
            Dim a As Integer
            ' for loop execution 
            For a = 0 To items.Length - 1
                Dim item As String = pStaDataDict.GetXMLText(items(a).Trim())
                If Not String.IsNullOrWhiteSpace(item) Then
                    items(a) = item
                End If
            Next
            Dim topNode As TreeNode
            If previousNode IsNot Nothing Then
                If previousNode.Text.Equals(items(0).Trim()) Then
                    topNode = previousNode
                Else
                    topNode = tvXml.Nodes.Add(tree.Key, items(0).Trim())
                End If
                index += 1
            Else
                topNode = tvXml.Nodes.Add(tree.Key, items(0).Trim())
                index += 1
            End If
            Dim childNode As TreeNode = topNode.Nodes.Add(tree.Key, items(1).Trim())
            index += 1
            For Each leaf As XmlModels.TreeModel.Intervento In tree.Intervento
                Dim item As String = pStaDataDict.GetXMLText(leaf.Text)
                If String.IsNullOrWhiteSpace(item) Then
                    item = leaf.Text
                End If
                Dim node As TreeNode = childNode.Nodes.Add(leaf.Key, item)
            Next
            previousNode = topNode
        Next
    End Sub


    Private Sub tvXml_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles tvXml.AfterSelect
        Dim data As XmlModels.DetailModel.Intervento =
                pdetailModel.Intervento.FirstOrDefault(Function(p) p.Key.Equals(e.Node.Name))
        If data IsNot Nothing Then
            rtbBibliography.LoadFile(Path.Combine(DATA_DIRECTORY, data.Bibliografia))
            rtbDescription.LoadFile(Path.Combine(DATA_DIRECTORY, data.Descrizione))
            rtbSolutions.LoadFile(Path.Combine(DATA_DIRECTORY, data.Soluzioni))
            LoadComputation(data.Lavorazioni.Split(";"c).ToList())
            pSelectedTreeKey = e.Node.Parent.Name
            pSelectedNodeKey = e.Node.Name
        End If
    End Sub

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOk.Click
        If String.IsNullOrWhiteSpace(txtTitle.Text) Then
            MessageBox.Show("Please enter Title")
        End If
        Dim setting As SettingManager = New SettingManager()
        pSetting.StaSetting.out_Intervento = pSelectedNodeKey
        pSetting.StaSetting.out_tree = pSelectedTreeKey
        pSetting.StaSetting.inp_TipoElemento = pCategories.Select((Function(elements As EnumElements) elements.ToInt())).ToList()
        pSetting.StaSetting.inp_Requisitos = pMaterials.Select((Function(elements As EnumMaterials) elements.ToInt())).ToList()
        pSetting.StaSetting.inp_TipoVestizione = pSubCategories
        pSetting.StaSetting.inp_SubRequisitos = pSubRequisitos
        pSetting.StaSetting.inp_Descrizione = txtTitle.Text
        pSetting.StaSetting.inp_Titolo = New FileInfo(pFilePath).Name.Replace(".cds", "")
        pSetting.StaSetting.inp_CategoriaIntervento = pInterventionTypes.ToInt()
        pSetting.StaSetting.out_Note = txtNote.Text
        Dim result As Boolean = setting.SaveSetting(pFilePath, pSetting)
        If result Then
            MessageBox.Show("Saved Successfully")
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub dgvItems_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgvItems.CellValueChanged
        Dim row As Integer = e.RowIndex
        If row < 0 Then
            Return
        End If
        If row > pSetting.Stalavorazioni.Items.Count - 1 Then
            Return
        End If
        Dim item As STAItemModel = pSetting.Stalavorazioni.Items.Item(row)
        Dim number As Decimal = 0
        Dim quantity As Decimal = 0
        Dim price As Decimal = 0
        Decimal.TryParse(dgvItems.Rows(row).Cells(1).Value.ToString(), number)
        Decimal.TryParse(dgvItems.Rows(row).Cells(2).Value.ToString(), quantity)
        Decimal.TryParse(dgvItems.Rows(row).Cells(3).Value.ToString(), price)
        Dim total As Decimal = Math.Round(number*quantity*price, 2)
        dgvItems.Rows(row).Cells(4).Value = total
        item.N = number
        item.Quantita = quantity
        item.Prezzo = price
        item.Toatale = total
    End Sub


    Private Sub Number_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)
        If Not Char.IsControl(e.KeyChar) AndAlso Not Char.IsDigit(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub dgvItems_EditingControlShowing_1(sender As Object, e As DataGridViewEditingControlShowingEventArgs) _
        Handles dgvItems.EditingControlShowing
        RemoveHandler e.Control.KeyPress, New KeyPressEventHandler(AddressOf Number_KeyPress)
        If _
            dgvItems.CurrentCell.ColumnIndex = 1 Or dgvItems.CurrentCell.ColumnIndex = 2 Or
            dgvItems.CurrentCell.ColumnIndex = 3 Then
            Dim tb As TextBox = TryCast(e.Control, TextBox)
            If tb IsNot Nothing Then
                AddHandler tb.KeyPress, New KeyPressEventHandler(AddressOf Number_KeyPress)
            End If
        End If
    End Sub
End Class
