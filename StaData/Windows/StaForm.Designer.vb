﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSta))
        Me.tvXml = New System.Windows.Forms.TreeView()
        Me.tcMain = New System.Windows.Forms.TabControl()
        Me.tpDescription = New System.Windows.Forms.TabPage()
        Me.rtbDescription = New System.Windows.Forms.RichTextBox()
        Me.tpInterventionArea = New System.Windows.Forms.TabPage()
        Me.tpBibliography = New System.Windows.Forms.TabPage()
        Me.rtbBibliography = New System.Windows.Forms.RichTextBox()
        Me.tpSolution = New System.Windows.Forms.TabPage()
        Me.rtbSolutions = New System.Windows.Forms.RichTextBox()
        Me.tpComputation = New System.Windows.Forms.TabPage()
        Me.dgvItems = New System.Windows.Forms.DataGridView()
        Me.gridBDescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gridBNo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gridBQuantity = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gridBPrice = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gridBTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvDataA = New System.Windows.Forms.DataGridView()
        Me.gridADescription = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.gridANo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblDescription = New System.Windows.Forms.Label()
        Me.txtTitle = New System.Windows.Forms.TextBox()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.txtNote = New System.Windows.Forms.TextBox()
        Me.lblNote = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.tcMain.SuspendLayout()
        Me.tpDescription.SuspendLayout()
        Me.tpBibliography.SuspendLayout()
        Me.tpSolution.SuspendLayout()
        Me.tpComputation.SuspendLayout()
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDataA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tvXml
        '
        Me.tvXml.HideSelection = False
        Me.tvXml.Location = New System.Drawing.Point(11, 91)
        Me.tvXml.Name = "tvXml"
        Me.tvXml.Size = New System.Drawing.Size(263, 488)
        Me.tvXml.TabIndex = 0
        '
        'tcMain
        '
        Me.tcMain.Controls.Add(Me.tpDescription)
        Me.tcMain.Controls.Add(Me.tpInterventionArea)
        Me.tcMain.Controls.Add(Me.tpBibliography)
        Me.tcMain.Controls.Add(Me.tpSolution)
        Me.tcMain.Controls.Add(Me.tpComputation)
        Me.tcMain.Location = New System.Drawing.Point(281, 91)
        Me.tcMain.Name = "tcMain"
        Me.tcMain.SelectedIndex = 0
        Me.tcMain.Size = New System.Drawing.Size(759, 387)
        Me.tcMain.TabIndex = 1
        '
        'tpDescription
        '
        Me.tpDescription.Controls.Add(Me.rtbDescription)
        Me.tpDescription.Location = New System.Drawing.Point(4, 26)
        Me.tpDescription.Name = "tpDescription"
        Me.tpDescription.Padding = New System.Windows.Forms.Padding(3)
        Me.tpDescription.Size = New System.Drawing.Size(751, 357)
        Me.tpDescription.TabIndex = 0
        Me.tpDescription.Text = "Description"
        Me.tpDescription.UseVisualStyleBackColor = True
        '
        'rtbDescription
        '
        Me.rtbDescription.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbDescription.Location = New System.Drawing.Point(3, 3)
        Me.rtbDescription.Name = "rtbDescription"
        Me.rtbDescription.ReadOnly = True
        Me.rtbDescription.Size = New System.Drawing.Size(745, 351)
        Me.rtbDescription.TabIndex = 0
        Me.rtbDescription.Text = ""
        '
        'tpInterventionArea
        '
        Me.tpInterventionArea.Location = New System.Drawing.Point(4, 25)
        Me.tpInterventionArea.Name = "tpInterventionArea"
        Me.tpInterventionArea.Padding = New System.Windows.Forms.Padding(3)
        Me.tpInterventionArea.Size = New System.Drawing.Size(751, 358)
        Me.tpInterventionArea.TabIndex = 1
        Me.tpInterventionArea.Text = "Intervention Area"
        Me.tpInterventionArea.UseVisualStyleBackColor = True
        '
        'tpBibliography
        '
        Me.tpBibliography.Controls.Add(Me.rtbBibliography)
        Me.tpBibliography.Location = New System.Drawing.Point(4, 25)
        Me.tpBibliography.Name = "tpBibliography"
        Me.tpBibliography.Padding = New System.Windows.Forms.Padding(3)
        Me.tpBibliography.Size = New System.Drawing.Size(751, 358)
        Me.tpBibliography.TabIndex = 2
        Me.tpBibliography.Text = "Bibliography"
        Me.tpBibliography.UseVisualStyleBackColor = True
        '
        'rtbBibliography
        '
        Me.rtbBibliography.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbBibliography.Location = New System.Drawing.Point(3, 3)
        Me.rtbBibliography.Name = "rtbBibliography"
        Me.rtbBibliography.ReadOnly = True
        Me.rtbBibliography.Size = New System.Drawing.Size(745, 352)
        Me.rtbBibliography.TabIndex = 0
        Me.rtbBibliography.Text = ""
        '
        'tpSolution
        '
        Me.tpSolution.Controls.Add(Me.rtbSolutions)
        Me.tpSolution.Location = New System.Drawing.Point(4, 25)
        Me.tpSolution.Name = "tpSolution"
        Me.tpSolution.Padding = New System.Windows.Forms.Padding(3)
        Me.tpSolution.Size = New System.Drawing.Size(751, 358)
        Me.tpSolution.TabIndex = 3
        Me.tpSolution.Text = "Solutions"
        Me.tpSolution.UseVisualStyleBackColor = True
        '
        'rtbSolutions
        '
        Me.rtbSolutions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtbSolutions.Location = New System.Drawing.Point(3, 3)
        Me.rtbSolutions.Name = "rtbSolutions"
        Me.rtbSolutions.ReadOnly = True
        Me.rtbSolutions.Size = New System.Drawing.Size(745, 352)
        Me.rtbSolutions.TabIndex = 0
        Me.rtbSolutions.Text = ""
        '
        'tpComputation
        '
        Me.tpComputation.Controls.Add(Me.dgvItems)
        Me.tpComputation.Controls.Add(Me.dgvDataA)
        Me.tpComputation.Location = New System.Drawing.Point(4, 25)
        Me.tpComputation.Name = "tpComputation"
        Me.tpComputation.Padding = New System.Windows.Forms.Padding(3)
        Me.tpComputation.Size = New System.Drawing.Size(751, 358)
        Me.tpComputation.TabIndex = 4
        Me.tpComputation.Text = "Computation"
        Me.tpComputation.UseVisualStyleBackColor = True
        '
        'dgvItems
        '
        Me.dgvItems.AllowUserToAddRows = False
        Me.dgvItems.AllowUserToDeleteRows = False
        Me.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvItems.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gridBDescription, Me.gridBNo, Me.gridBQuantity, Me.gridBPrice, Me.gridBTotal})
        Me.dgvItems.Location = New System.Drawing.Point(6, 190)
        Me.dgvItems.Name = "dgvItems"
        Me.dgvItems.RowHeadersVisible = False
        Me.dgvItems.RowHeadersWidth = 51
        Me.dgvItems.RowTemplate.Height = 24
        Me.dgvItems.Size = New System.Drawing.Size(739, 159)
        Me.dgvItems.TabIndex = 1
        '
        'gridBDescription
        '
        Me.gridBDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.gridBDescription.HeaderText = "Description"
        Me.gridBDescription.MinimumWidth = 6
        Me.gridBDescription.Name = "gridBDescription"
        Me.gridBDescription.ReadOnly = True
        Me.gridBDescription.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.gridBDescription.Width = 105
        '
        'gridBNo
        '
        Me.gridBNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.gridBNo.HeaderText = "No"
        Me.gridBNo.MinimumWidth = 6
        Me.gridBNo.Name = "gridBNo"
        Me.gridBNo.Width = 54
        '
        'gridBQuantity
        '
        Me.gridBQuantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.gridBQuantity.HeaderText = "Quantity"
        Me.gridBQuantity.MinimumWidth = 6
        Me.gridBQuantity.Name = "gridBQuantity"
        Me.gridBQuantity.Width = 90
        '
        'gridBPrice
        '
        Me.gridBPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.gridBPrice.HeaderText = "Price"
        Me.gridBPrice.MinimumWidth = 6
        Me.gridBPrice.Name = "gridBPrice"
        Me.gridBPrice.Width = 66
        '
        'gridBTotal
        '
        Me.gridBTotal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.gridBTotal.HeaderText = "Total"
        Me.gridBTotal.MinimumWidth = 6
        Me.gridBTotal.Name = "gridBTotal"
        Me.gridBTotal.ReadOnly = True
        Me.gridBTotal.Width = 67
        '
        'dgvDataA
        '
        Me.dgvDataA.AllowUserToAddRows = False
        Me.dgvDataA.AllowUserToDeleteRows = False
        Me.dgvDataA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDataA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.gridADescription, Me.gridANo})
        Me.dgvDataA.Location = New System.Drawing.Point(6, 6)
        Me.dgvDataA.Name = "dgvDataA"
        Me.dgvDataA.RowHeadersVisible = False
        Me.dgvDataA.RowHeadersWidth = 51
        Me.dgvDataA.RowTemplate.Height = 24
        Me.dgvDataA.Size = New System.Drawing.Size(739, 159)
        Me.dgvDataA.TabIndex = 0
        '
        'gridADescription
        '
        Me.gridADescription.HeaderText = "Description"
        Me.gridADescription.MinimumWidth = 6
        Me.gridADescription.Name = "gridADescription"
        Me.gridADescription.ReadOnly = True
        Me.gridADescription.Width = 125
        '
        'gridANo
        '
        Me.gridANo.HeaderText = "No"
        Me.gridANo.MinimumWidth = 6
        Me.gridANo.Name = "gridANo"
        Me.gridANo.ReadOnly = True
        Me.gridANo.Width = 125
        '
        'lblDescription
        '
        Me.lblDescription.AutoSize = True
        Me.lblDescription.Location = New System.Drawing.Point(11, 39)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(76, 17)
        Me.lblDescription.TabIndex = 3
        Me.lblDescription.Text = "Description"
        '
        'txtTitle
        '
        Me.txtTitle.Location = New System.Drawing.Point(15, 62)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(260, 24)
        Me.txtTitle.TabIndex = 4
        '
        'btnOk
        '
        Me.btnOk.Location = New System.Drawing.Point(774, 585)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(105, 37)
        Me.btnOk.TabIndex = 8
        Me.btnOk.Text = "Ok"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(885, 585)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(105, 37)
        Me.btnCancel.TabIndex = 9
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'txtNote
        '
        Me.txtNote.Location = New System.Drawing.Point(285, 502)
        Me.txtNote.Multiline = True
        Me.txtNote.Name = "txtNote"
        Me.txtNote.Size = New System.Drawing.Size(751, 78)
        Me.txtNote.TabIndex = 11
        '
        'lblNote
        '
        Me.lblNote.AutoSize = True
        Me.lblNote.Location = New System.Drawing.Point(285, 480)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(37, 17)
        Me.lblNote.TabIndex = 12
        Me.lblNote.Text = "Note"
        '
        'Button1
        '
        Me.Button1.FlatAppearance.BorderSize = 0
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.Location = New System.Drawing.Point(997, 586)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(40, 36)
        Me.Button1.TabIndex = 13
        Me.Button1.UseVisualStyleBackColor = True
        '
        'frmSta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1051, 636)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblNote)
        Me.Controls.Add(Me.txtNote)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOk)
        Me.Controls.Add(Me.txtTitle)
        Me.Controls.Add(Me.lblDescription)
        Me.Controls.Add(Me.tcMain)
        Me.Controls.Add(Me.tvXml)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmSta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "STA Data"
        Me.tcMain.ResumeLayout(False)
        Me.tpDescription.ResumeLayout(False)
        Me.tpBibliography.ResumeLayout(False)
        Me.tpSolution.ResumeLayout(False)
        Me.tpComputation.ResumeLayout(False)
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDataA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tvXml As TreeView
    Friend WithEvents tcMain As TabControl
    Friend WithEvents lblDescription As Label
    Friend WithEvents txtTitle As TextBox
    Friend WithEvents tpDescription As TabPage
    Friend WithEvents tpInterventionArea As TabPage
    Friend WithEvents tpBibliography As TabPage
    Friend WithEvents tpSolution As TabPage
    Friend WithEvents tpComputation As TabPage
    Friend WithEvents rtbDescription As RichTextBox
    Friend WithEvents rtbBibliography As RichTextBox
    Friend WithEvents rtbSolutions As RichTextBox
    Friend WithEvents dgvItems As DataGridView
    Friend WithEvents dgvDataA As DataGridView
    Friend WithEvents gridADescription As DataGridViewTextBoxColumn
    Friend WithEvents gridANo As DataGridViewTextBoxColumn
    Friend WithEvents btnOk As Button
    Friend WithEvents btnCancel As Button
    Friend WithEvents gridBDescription As DataGridViewTextBoxColumn
    Friend WithEvents gridBNo As DataGridViewTextBoxColumn
    Friend WithEvents gridBQuantity As DataGridViewTextBoxColumn
    Friend WithEvents gridBPrice As DataGridViewTextBoxColumn
    Friend WithEvents gridBTotal As DataGridViewTextBoxColumn
    Friend WithEvents txtNote As TextBox
    Friend WithEvents lblNote As Label
    Friend WithEvents Button1 As Button
End Class
